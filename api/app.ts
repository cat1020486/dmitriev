import 'dotenv/config'
import express, { Request, Response } from "express";
import { body, ValidationError, validationResult } from 'express-validator';
import db, { Task , User, Session} from '../prisma';
import cors from 'cors'
import bcrypt from 'bcrypt'
import { nanoid } from 'nanoid';
import {sign , unsign} from './signature'


const app = express()
app.use(cors())

app.use('/api', express.json({ limit: '5kb' }))

type TaskDto = {
    name: string
    done: boolean
}

app.get('/api/task', async (req, res) => {
    const { take, skip } = req.query
    const tasks = await db.task.findMany(
        (take && skip) ? { take: +take, skip: +skip } : undefined
    )
    res.send(tasks)
})

app.post('/api/task',
    body('name').isLength({ min: 1, max: 100 }),
    body('done').isBoolean(),
    async (req: Request<{}, {}, TaskDto>, res: Response<Task | { errors: ValidationError[] }>) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const { name, done } = req.body
        const task = await db.task.create({
            data: {
                done,
                name
            }
        })
        res.send(task)
    })

app.delete('/api/task/:id', async (req, res) => {
    const { id } = req.params
    await db.task.delete({ where: { id } })
    // db.task.update({where: {id}}, )
    res.send({ success: true })
})


// secure route
app.patch('/user/:id', checkCookie, async (req, res) => {
  const { id } = req.params
  const { name, email } = req.body
  const user = req.user
  if (user && user.id === id)
    await db.user.update({where: {id: user.id}, data: {name, email}})
  res.status(200).send({ status: 'success' })
})

app.post('/login', async (req, res) => {
  const { email, password } = req.body
  const userByEmail = await db.user.findUnique({ where: { email } })
  if (userByEmail) {
    if (await bcrypt.compare(password, userByEmail.passwordHash)) {
      // sessions
      createSession(res, userByEmail.id)
    } else {
      res.status(401).send({ status: 'Incorrect credentials' })
    }
  } else {
    res.status(401).send({ status: 'Incorrect credentials' })
  }
})

app.post('/signup',
  body('email').isEmail(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { name, email, password } = req.body
    try {
      const passwordHash = await bcrypt.hash(password, 10)
      const userByEmail = await db.user.findUnique({ where: { email } })
      if (!userByEmail) {
        const newUser = await db.user.create({ 
            data: {
                name, 
                email, 
                passwordHash
            }}
        )
        res.status(201).send(newUser)
      } else {
        res.status(400).send({ message: 'This email is not unique' })
      }
    } catch (error) {
      console.error(error)
      res.status(500).send({ message: 'Server error' })
    }
  })

async function createSession(res, userId) {
    const sid = nanoid() // Example: WKlEdLMwTjJE9cSfOuniI
    if (process.env.SECRET) {
    const token = sign(sid, process.env.SECRET) // WKlEdLMwTjJE9cSfOuniI.KKF2QT4fwpMeJf36POk6yJV_adQssw5c // DB + JWT
    res.cookie('sid', token, { httpOnly: true, maxAge: 2 * 24 * 60 * 60 * 1000 })
    const session = await db.session.create({
        data: {
            sessionToken: sid,
            userId,
            expires: new Date(Date.now() + 2*24*60*60*1000),
        }}
    )
    res.send(session)
  }
}
  
  async function checkCookie(req, res, next) {
    const token = req.cookies['Token']
    if (token &&  process.env.SECRET) {
      const sid = unsign(token, process.env.SECRET)
      if (sid) {
        const session = await db.session.findUnique({ where: { sid } })
        if (session) {
          req.user = { id: session.userId }
          return next()
        }
      }
    }
    res.status(403).send({ status: 'Wrong token' })
  }
  


app.listen(4000)